# Aqoda

Hotel Self Check-in & Check-out System

## คำอธิบายโจทย์

ให้ออกแบบและเขียนโปรแกรมตามสถานการณ์ที่กำหนด โดยโปรแกรมเป็นโปรแกรมที่ทำงานบน Command Line โดยอ่านไฟล์ข้อมูลนำเข้า และแสดงผลที่ Standard Output (ตามตัวอย่าง)

ทั้งนี้ ให้ผู้รับการทดสอบเขียนโค้ดเสมือนว่างานนี้เป็นงานที่จะต้องใช้จริงบน Production โดยสิ่งที่ผู้ตรวจสนใจคือ

1. ความถูกต้องของ Logic
2. ความถูกต้องแหละเหมาะสมของการใช้ Syntax, Idioms, Expression ตามภาษาโปรแกรม และ Coding Convention
3. การออกแบบโครงสร้างโปรแกรม ไม่ว่าจะเป็น Object-Oriented หรีือ Functional

## Scenario

เมื่อแขกเดินทางมาถึงที่พัก จะต้องแจ้งเลขห้อง ชื่อและอายุ เพื่อทำ Check-in เข้าพัก จากนั้นระบบจะออก Keycard ให้แขกเพื่อใช้ในการเปิดประตูห้องพัก

และเมื่อแขกต้องการ Check-out จะต้องนำ Keycard มาคืนพร้อมแจ้งชื่อ ให้ตรงกับที่ระบุไว้ใน Keycard ตอน Check-in ถึงจะสามารถ Check-out ได้

โดยโปรแกรมจะต้องรอบรับการใช้งานขั้นต่ำ ดังนี้:

1. สร้างโรงแรมโดยกำหนดจำนวนชั้นและจำนวนห้องต่อชั้นได้
   เลขห้องจะต้องเป็นเลข 3 หลัก ตัวแรกคือเลขชั้น 2 ตัวที่เหลือคือเลขห้อง เริ่มจาก 01
2. ดูรายการห้องว่างทั้งหมดได้
3. ดูรายชื่อแขกทั้งหมดได้
4. ดูรายชื่อแขกโดยกำหนดช่วงอายุได้
5. ดูชื่อแขกในห้องพักที่ระบุได้

## ตัวอย่างการรันโปรแกรม

เมื่อรันโดยข้อมูลนำเข้าใน `input.txt`

```text
$ node main.js
Hotel created with 2 floor(s), 3 room(s) per floor.
Room 203 is booked by Thor with keycard number 1.
Room 101 is booked by PeterParker with keycard number 2.
Room 102 is booked by StephenStrange with keycard number 3.
Room 201 is booked by TonyStark with keycard number 4.
Room 202 is booked by TonyStark with keycard number 5.
Cannot book room 203 for TonyStark, The room is currently booked by Thor.
103
Room 201 is checkout.
Room 103 is booked by TonyStark with keycard number 4.
Hotel is fully booked.
Only Thor can checkout with keycard number 1.
Room 202 is checkout.
Room 103 is checkout.
Thor, PeterParker, StephenStrange
Thor
PeterParker
```

## วิธีการทำโจทย์

1. เลือกภาษาที่ใช้เขียนโค้ด (หากเป็น JavaScript + Node.js จะดีมาก)
2. แปลงโค้ดในไฟล์ main.js เป็นภาษาที่เลือก รวมถึงพัฒนาให้อ่านคำสั่งต่างๆ จาก input.txt ได้สมบูรณ์ 
3. ทดสอบการทำงานของโปรแกรมโดยใช้ข้อมูลนำเข้าจากไฟล์  input.txt และต้องได้ผลลัพธ์เหมือนไฟล์ ouput.txt

*** มีเวลาทำ 48 ชั่วโมง เริ่มนับเวลาจากที่ได้รับโจทย์ ***